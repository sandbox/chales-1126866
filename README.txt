$Id$

This module allows you to easily configure and output a block from the twitter_pull module which is a dependency. http://drupal.org/project/twitter_pull

Installation
------------
1. Copy the twitter_pull_config folder to your sites/all/modules directory. See http://drupal.org/node/70151 for further information.

2. At Administer -> Site building -> Modules (admin/build/modules) enable the module.

3. Configure the module settings at Administer -> Site configuration -> Twitter pull config (admin/settings/twitter_pull_config).

4. Configure user permissions in Administer -> User management -> Permissions ->
twitter_pull_config module: administer twitter_pull_config
(admin/user/permissions#module-twitter_pull_config)


Features
--------
- Allows you to set configuration options for twitter_pull through the Drupal UI.
- A new block will be located on your blocks admin page called "Twitter Pull Block."
